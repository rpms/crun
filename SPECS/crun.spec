Summary: OCI runtime written in C
Name: crun
Version: 0.18
Release: 3%{?dist}
Source0: https://github.com/containers/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz
Patch0: 0001-revert-tests-build-init-always-statically.patch
Patch1: 0001-spec-do-not-set-inheritable-capabilities.patch
Patch2: 0002-exec-cap-do-not-set-inheritable-capabilities.patch
License: GPLv2+
URL: https://github.com/containers/crun
# https://fedoraproject.org/wiki/PackagingDrafts/Go#Go_Language_Architectures
ExclusiveArch: %{go_arches}
# We always run autogen.sh
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc
BuildRequires: python3
BuildRequires: git
BuildRequires: libcap-devel
BuildRequires: systemd-devel
BuildRequires: yajl-devel
BuildRequires: libseccomp-devel
BuildRequires: libselinux-devel
BuildRequires: python3-libmount
BuildRequires: libtool
BuildRequires: go-md2man
Provides: oci-runtime = 2

%description
crun is a runtime for running OCI containers

%prep
%autosetup -Sgit -n %{name}-%{version}

%build
export CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"
./autogen.sh
%configure --disable-silent-rules

%make_build

%install
%make_install
rm -rf $RPM_BUILD_ROOT/usr/lib*

%files
%license COPYING
%{_bindir}/%{name}
%{_mandir}/man1/*

%changelog
* Tue Apr 05 2022 Jindrich Novy <jnovy@redhat.com> - 0.18-3
- fix CVE-2022-27650
- Related: #2061390

* Fri Feb 19 2021 Jindrich Novy <jnovy@redhat.com> - 0.18-2
- allow to build without glibc-static (thanks to Giuseppe Scrivano)
- Related: #1883490

* Fri Feb 19 2021 Jindrich Novy <jnovy@redhat.com> - 0.18-1
- update to https://github.com/containers/crun/releases/tag/0.18
- Related: #1883490

* Fri Jan 22 2021 Jindrich Novy <jnovy@redhat.com> - 0.17-1
- update to https://github.com/containers/crun/releases/tag/0.17
- Related: #1883490

* Thu Dec 03 2020 Jindrich Novy <jnovy@redhat.com> - 0.16-2
- exclude i686 because of build failures
- Related: #1883490

* Wed Nov 25 2020 Jindrich Novy <jnovy@redhat.com> - 0.16-1
- update to https://github.com/containers/crun/releases/tag/0.16
- Related: #1883490

* Wed Nov 04 2020 Jindrich Novy <jnovy@redhat.com> - 0.15.1-1
- update to https://github.com/containers/crun/releases/tag/0.15.1
- Related: #1883490

* Thu Oct 29 2020 Jindrich Novy <jnovy@redhat.com> - 0.15-2
- synchronize with stream-container-tools-rhel8
- Related: #1883490

* Wed Oct 21 2020 Jindrich Novy <jnovy@redhat.com> - 0.15-1
- synchronize with stream-container-tools-rhel8
- Related: #1883490

* Tue Aug 11 2020 Jindrich Novy <jnovy@redhat.com> - 0.14.1-2
- use proper CFLAGS
- Related: #1821193

* Wed Jul 08 2020 Jindrich Novy <jnovy@redhat.com> - 0.14.1-1
- update to https://github.com/containers/crun/releases/tag/v0.14.1
- Related: #1821193

* Thu Jul 02 2020 Jindrich Novy <jnovy@redhat.com> - 0.14-1
- update to https://github.com/containers/crun/releases/tag/v0.14
- Related: #1821193

* Tue Jun 16 2020 Giuseppe Scrivano <gscrivan@redhat.com> - 0.13-1
- initial import
